import os
from jinja2 import ChoiceLoader, FileSystemLoader
from jupyterhub.handlers import BaseHandler
from jupyterhub.handlers.login import LoginHandler
from jupyterhub.utils import admin_only
from jupyterhub.utilscustom import convertDateTimeToKoreaSeoul, create_user_from_nativeauthenticator, set_jupyterhub_user_admin, get_jupyterhub_users

from tornado import web
from tornado.escape import url_escape
from tornado.httputil import url_concat

# from .orm import UserInfo
from jupyterhub.ormnative import UserInfo

TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), 'templates')


class LocalBase(BaseHandler):
    def __init__(self, *args, **kwargs):
        self._loaded = False
        super().__init__(*args, **kwargs)

    def _register_template_path(self):
        if self._loaded:
            return
        self.log.debug('Adding %s to template path', TEMPLATE_DIR)
        loader = FileSystemLoader([TEMPLATE_DIR])
        env = self.settings['jinja2_env']
        previous_loader = env.loader
        env.loader = ChoiceLoader([previous_loader, loader])
        self._loaded = True


class SignUpHandler(LocalBase):
    """Render the sign in page."""
    async def get(self):
        self._register_template_path()
        html = self.render_template(
            'signup-custom.html',
            ask_email=self.authenticator.ask_email_on_signup,
            two_factor_auth=self.authenticator.allow_2fa,
        )
        self.finish(html)

    def get_result_message(self, user):
        alert = 'alert-info'
        message = 'Your information have been sent to the admin'

        if self.authenticator.open_signup:
            alert = 'alert-success'
            message = ('The signup was successful. You can now go to '
                       'home page and log in the system')
        if not user:
            alert = 'alert-danger'
            pw_len = self.authenticator.minimum_password_length

            if pw_len:
                message = ("Something went wrong. Be sure your password has "
                           "at least {} characters, doesn't have spaces or "
                           "commas and is not too common.").format(pw_len)

            else:
                message = ("Something went wrong. Be sure your password "
                           " doesn't have spaces or commas and is not too "
                           "common.")

        return alert, message

    async def post(self):
        user_info = {
            'username': self.get_body_argument('username', strip=False),
            'pw': self.get_body_argument('pw', strip=False),
            'user_real_name': self.get_body_argument('user_real_name', strip=False),
            'email': self.get_body_argument('email', '', strip=False),
            'phone': self.get_body_argument('phone', '', strip=False),
            'organization': self.get_body_argument('organization', '', strip=False),
            'has_2fa': bool(self.get_body_argument('2fa', '', strip=False)),
        }
        user = self.authenticator.get_or_create_user(**user_info)

        alert, message = self.get_result_message(user)

        otp_secret, user_2fa = '', ''
        if user:
            otp_secret = user.otp_secret
            user_2fa = user.has_2fa

        await create_user_from_nativeauthenticator(self, **user_info)

        html = self.render_template(
            'signup-custom.html',
            ask_email=self.authenticator.ask_email_on_signup,
            result_message=message,
            alert=alert,
            two_factor_auth=self.authenticator.allow_2fa,
            two_factor_auth_user=user_2fa,
            two_factor_auth_value=otp_secret,
        )
        self.finish(html)


class UserEditHandler(LocalBase):
    """Render the User Edit page."""
    async def get(self, slug):
        user = self.current_user
        self._register_template_path()
        html = self.render_template(
            'user-edit-custom.html',
            user = user,
            ask_email=self.authenticator.ask_email_on_signup,
            two_factor_auth=self.authenticator.allow_2fa,
            userInformation=UserInfo.find(self.db, slug),
            convertDateTimeToKoreaSeoul=convertDateTimeToKoreaSeoul
        )
        self.finish(html)

    @web.authenticated
    async def post(self, slug):
        user = UserInfo.find(self.db, slug)
        new_user_real_name = self.get_body_argument('user_real_name', strip=False)
        new_email = self.get_body_argument('email', strip=False)
        new_phone = self.get_body_argument('phone', strip=False)
        new_organization = self.get_body_argument('organization', strip=False)
        self.authenticator.change_profile(user.username, new_user_real_name, new_email, new_phone, new_organization)

        alert = 'alert-success'
        message = ('This user has updated their profile')

        html = self.render_template(
            'user-edit-custom.html',
            ask_email=self.authenticator.ask_email_on_signup,
            two_factor_auth=self.authenticator.allow_2fa,
            userInformation=UserInfo.find(self.db, slug),
            alert=alert,
            result_message=message,
            convertDateTimeToKoreaSeoul=convertDateTimeToKoreaSeoul
        )
        self.finish(html)

#         self.redirect('/authorize')

class AuthorizationHandler(LocalBase):
    """Render the sign in page."""
    @admin_only
    async def get(self):
        self._register_template_path()
#        todo: 1.0.0 대응
        users_jupyterhub=get_jupyterhub_users(self)
        UserInfo.update_admin(self.db, users_jupyterhub)
        html = self.render_template(
            'autorization-area-custom.html',
            ask_email=self.authenticator.ask_email_on_signup,
            users=self.db.query(UserInfo).all(),
            users_jupyterhub=users_jupyterhub,
            convertDateTimeToKoreaSeoul=convertDateTimeToKoreaSeoul
        )
        self.finish(html)

class ChangeUserAdminHandler(LocalBase):
    @admin_only
    async def get(self, slug):
        print('ChangeUserAdminHandler')
        user = UserInfo.find(self.db, slug)
        user.admin = not user.admin
        await set_jupyterhub_user_admin(self, slug)
        self.redirect('/user-edit/%s' % slug)

class ChangeAuthorizationHandler(LocalBase):
    @admin_only
    async def get(self, slug):
        UserInfo.change_authorization(self.db, slug)
        self.redirect('/user-edit/%s' % slug)

class ChangePasswordHandler(LocalBase):
    """Render the reset password page."""

    @web.authenticated
    async def get(self):
        self._register_template_path()
        html = self.render_template('change-password.html')
        self.finish(html)

    @web.authenticated
    async def post(self):
        user = await self.get_current_user()
        new_password = self.get_body_argument('password', strip=False)
        self.authenticator.change_password(user.name, new_password)

        html = self.render_template(
            'change-password.html',
            result_message='Your password has been changed successfully',
        )
        self.finish(html)


class LoginHandler(LoginHandler, LocalBase):

    def _render(self, login_error=None, username=None):
        self._register_template_path()
        return self.render_template(
            'native-login-custom.html',
            next=url_escape(self.get_argument('next', default='')),
            username=username,
            login_error=login_error,
            custom_html=self.authenticator.custom_html,
            login_url=self.settings['login_url'],
            two_factor_auth=self.authenticator.allow_2fa,
            authenticator_login_url=url_concat(
                self.authenticator.login_url(self.hub.base_url),
                {'next': self.get_argument('next', '')},
            ),
        )
